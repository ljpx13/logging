package logging

import (
	"testing"

	"gitlab.com/ljpx13/test"
	"gitlab.com/ljpx13/test/verbs/is"
)

func TestTestLoggerAssertLoggedFailure(t *testing.T) {
	// Arrange.
	logger := NewTestLogger()
	recorder := test.NewMockT()

	// Act.
	logger.Printf("Hello, World!")
	logger.AssertLogged(recorder, "Hello, Worlds!")

	// Assert.
	test.That(t, recorder.DidFail(), is.True())
}

func TestTestLoggerAssertLoggedSuccess(t *testing.T) {
	// Arrange.
	logger := NewTestLogger()
	recorder := test.NewMockT()

	// Act.
	logger.Printf("Hello, World!")
	logger.AssertLogged(recorder, "Hello, World!")

	// Assert.
	test.That(t, recorder.DidFail(), is.False())
}

func TestTestLoggerAssertPatternLoggedFailure(t *testing.T) {
	// Arrange.
	logger := NewTestLogger()
	recorder := test.NewMockT()

	// Act.
	logger.Printf("Hello, World!")
	logger.AssertPatternLogged(recorder, `^Hello, w.{5}$`)

	// Assert.
	test.That(t, recorder.DidFail(), is.True())
}

func TestTestLoggerAssertPatternLoggedSuccess(t *testing.T) {
	// Arrange.
	logger := NewTestLogger()
	recorder := test.NewMockT()

	// Act.
	logger.Printf("Hello, World!")
	logger.AssertPatternLogged(recorder, `^Hello, W.{5}$`)

	// Assert.
	test.That(t, recorder.DidFail(), is.False())
}
