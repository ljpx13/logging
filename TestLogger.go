package logging

import (
	"fmt"
	"regexp"
	"sync"

	"gitlab.com/ljpx13/test"
)

// TestLogger is a dummy implementation of Logger that simply records the
// messages logged to it.
type TestLogger struct {
	messages map[string]int
	mx       *sync.RWMutex
}

var _ Logger = &TestLogger{}

// NewTestLogger creates a new, empty TestLogger.
func NewTestLogger() *TestLogger {
	return &TestLogger{
		messages: make(map[string]int),
		mx:       &sync.RWMutex{},
	}
}

// Printf records a message into the TestLogger.
func (d *TestLogger) Printf(format string, v ...interface{}) {
	msg := fmt.Sprintf(format, v...)

	d.mx.Lock()
	defer d.mx.Unlock()

	d.messages[msg]++
}

// AssertLogged asserts that the message specified was logged to the
// TestLogger.
func (d *TestLogger) AssertLogged(t test.TLike, format string, v ...interface{}) {
	t.Helper()

	msg := fmt.Sprintf(format, v...)

	d.mx.RLock()
	defer d.mx.RUnlock()

	c, ok := d.messages[msg]
	if !ok || c < 1 {
		t.Fatalf("expected message:\n\n%v\n\nto have been logged, but hadn't", msg)
	}
}

// AssertPatternLogged asserts that at least one message logged matches the
// provided pattern.
func (d *TestLogger) AssertPatternLogged(t test.TLike, pattern string) {
	t.Helper()

	rx := regexp.MustCompile(pattern)

	d.mx.RLock()
	defer d.mx.RUnlock()

	for v := range d.messages {
		if rx.Match([]byte(v)) {
			return
		}
	}

	t.Fatalf("expected a message with pattern:\n\n%v\n\nto have been logged, but hadn't", pattern)
}
